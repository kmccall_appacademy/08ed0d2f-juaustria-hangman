class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board
    @misses = []
    @hangman_img = [
      " ____    ",
      "|        ",
      "|        ",
      "|        ",
      "|        "
    ]
  end

  def setup
    sec_length = @referee.pick_secret_word
    @guesser.register_secret_length(sec_length)
    @board = Array.new(sec_length)
  end

  def take_turn
    a_guess = @guesser.guess(@board)
    matches = @referee.check_guess(a_guess)
    update_board(a_guess, matches)
    @misses = @referee.misses
    @guesser.misses = @referee.misses
    @guesser.handle_response(a_guess, matches)
  end

  def update_board(char, matches)
    matches.each { |pos| @board[pos] = char }
  end

  def update_img
    case @misses.length
    when 1
      @hangman_img[1] = "|  (o o) "
    when 2
      @hangman_img[2] = "|    |   "
      @hangman_img[3] = "|    |   "
    when 3
      @hangman_img[2] = "|  --|   "
    when 4
      @hangman_img[2] = "|  --|-- "
    when 5
      @hangman_img[4] = "|   -'   "
    when 6
      @hangman_img[1] = "|  (X X) "
      @hangman_img[4] = "|   -'-  "
    else
      true
    end
  end

  def render
    puts 'Missed Letters'
    p @misses

    board_display = ''
    print "The Secret Word : #{board_display}"

    @board.each do |char|
      board_display << if char.nil?
                         '_'
                       else
                         char
                       end
    end
    p board_display

    update_img
    @hangman_img.each { |line| p line }
  end

  def over?
    !@board.include?(nil) || @referee.misses.length >= 6
  end

  def play
    setup

    until over?
      render
      take_turn
      render
    end
  end
end

class HumanPlayer
  attr_accessor :misses
  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
    @misses = []
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
    @secret_length
  end

  def check_guess(guess)
    @misses << guess unless @secret_word.include?(guess)
    matched_index = []
    @secret_word.chars.each_with_index do |char, idx|
      matched_index << idx if char == guess
    end
    matched_index
  end

  def guess(board)
    human_guess = ''
    p 'Please pick a letter?'

    human_guess = gets.chomp

    if human_guess.length != 1
      guess(board)
    elsif board.include?(human_guess.downcase) || @misses.include?(human_guess.downcase)
      p 'You already picked this letter'
      guess(board)
    else
      return human_guess.downcase
    end
  end

  def handle_response(guess, pos_arr)
    # select words with matching words in index positions
    @candidate_words.select! do |word|
      output = true
      pos_arr.each do |pos|
        output = false if guess != word[pos]
      end
      output
    end
    # reject words that have guess in non matching indexes
    @candidate_words.select! do |word|
      output = true
      word.chars.each_with_index do |char, idx|
        output = false if char == guess && !pos_arr.include?(idx)
      end
      output
    end
    # reject words without guessed letter
    @candidate_words.reject! do |word|
      word if word.include?(guess) && pos_arr.length.zero?
    end
  end

  def candidate_words
    @candidate_words.select { |word| word.length == @secret_length }
  end
end

class ComputerPlayer
  attr_accessor :misses
  def initialize(dictionary)
    @dictionary = dictionary
    @candidate_words = dictionary
    @misses = []
  end

  def pick_secret_word
    @secret_word = @dictionary.sample
    @secret_word.length
  end

  def register_secret_length(length)
    @secret_length = length
    @secret_length
  end

  def check_guess(guess)
    @misses << guess unless @secret_word.include?(guess)
    matched_index = []
    @secret_word.chars.each_with_index do |char, idx|
      matched_index << idx if char == guess
    end
    matched_index
  end

  def guess(board)
    most_frequent = ''
    letter_count = Hash.new(0)
    @candidate_words.join('').split('').each { |c| letter_count[c] += 1 }
    letter_count.keys.each do |key|
      if letter_count[key] > letter_count[most_frequent] && !board.include?(key)
        most_frequent = key
      end
    end
    most_frequent
  end

  def handle_response(guess, pos_arr)
    # select words with matching words in index positions
    @candidate_words.select! do |word|
      output = true
      pos_arr.each do |pos|
        output = false if guess != word[pos]
      end
      output
    end
    # reject words that have guess in non matching indexes
    @candidate_words.select! do |word|
      output = true
      word.chars.each_with_index do |char, idx|
        output = false if char == guess && !pos_arr.include?(idx)
      end
      output
    end
    # reject words without guessed letter
    @candidate_words.reject! do |word|
      word if word.include?(guess) && pos_arr.length.zero?
    end
  end

  def candidate_words
    @candidate_words.select { |word| word.length == @secret_length }
  end

end
